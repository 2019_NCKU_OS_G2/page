#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
// int getopt(int argc, char * const *argv, const char *optstring);
#define FILESIZE 500

void operation(int n);

int main(int argc, char*argv[]) {
    int ch;
    int n = 0;
    while ((ch = getopt(argc, argv, "n:")) != -1)
    {
        switch (ch) {
            case 'n':
                printf("HAVE option: -n\n");
                n = atoi(argv[2]);
                printf("n = %d\n", n);
                operation(n);
                break;
            case '?':
                printf("Unknown option: %c\n",(char)optopt);
                break;
        }
    }
}

void operation(int n){

    //  Read input file
    FILE *fin, *fout;

    fin = fopen("test.txt", "r");

    unsigned int data[FILESIZE];

    for(int i = 0; i < FILESIZE; i++){
        fscanf(fin, "%x", &data[i]);
    }
    //  printf("%u",data[1]);

    fclose(fin);

    //  Turn m and n into num of bytes
    int mBit = 32;  //  Bits of virtual memory
    int nBit = n;   //  Bits of page size
    //  printf("m=%d,n=%d\n", mByte, nByte);

    unsigned int numberMask = 0;
    unsigned int offsetMask = 0;

    // Create page number mask
    for(int i = 0; i < mBit - nBit; i++){
        numberMask = numberMask << 1;
        numberMask = numberMask + 1;
    }

    numberMask = numberMask << nBit;

    printf("numberMask: %x\n", numberMask);

    // Create offset mask
    for(int i = 0; i < nBit; i++){
        offsetMask = offsetMask << 1;
        offsetMask = offsetMask + 1;
    }

    printf("offsetMask: %x\n", offsetMask);

    // Use &(and) operator to calculate page number and offset
    // Write into output file
    unsigned int number = 0;
    unsigned int offset = 0;

    fout = fopen("group2_ans.txt","w");

    for(int i = 0; i < FILESIZE; i++){
        number = numberMask & data[i];
        number = number >> nBit;
        offset = offsetMask & data[i];
        //  printf("data:0x%08x\n", data[i]);
        //  printf("number:%u,offset:%u\n", number,offset);
        fprintf(fout, "%u %u\n", number, offset);
    }

    fclose(fout);
}

#include "cachelab.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <getopt.h>
#include <time.h>
#include <math.h>

#define ADDRLEN 64 // Assume the address length is 64 bits

enum {invalid = 0, valid = 1};
int verbose_flag;
int time_sim;

/*
 * @brief Info of each entry of a cache
 * @param vld: Valid bit
 * @param tag: Tag of a entry in cache
 * @param set: Set of a entry in cache
 * @param blk: Block of a entry in cache
 * @param last_ref: The last referenced "time"
 */
typedef struct cache_line {
	int vld;
	unsigned long long tag;
	unsigned long long set;
	unsigned long long blk;
    unsigned int last_ref;
} cline;

/*
 * @brief Info of total memory access
 * @param hits: Number of hits
 * @param misses: Number of misses
 * @param evictions: Number of evictions
 */
typedef struct memory_access {
	int hits;
	int misses;
	int evictions;
} macs;

// +++++++++++++++++++++++++ Function Prototype +++++++++++++++++++++++++++++ //

cline* create_cache(int, int, int);
int cache_LRU(cline*, int, int, int);
void cache_access(macs*, cline*, unsigned long long, unsigned int, int, int, int);
void show_cache_content(cline*, int, int, int);

// ++++++++++++++++++++++++++++ Main Function +++++++++++++++++++++++++++++++ //

int main(int argc, char*argv[]) {


// +++++++++++++++++++++++++++++ Get Options ++++++++++++++++++++++++++++++++ //
    
    /* Input form
	 *
	 * ./csim [-hv] -s <s> -E <E> -b <b> -t <tracefile>
	 *
	 * -h: Optional help flag that prints usage info
	 * -v: Optional verbose flag that displays trace info
	 * -s <s>: Number of set index bits (S = 2^s is the number of sets)
	 * -E <E>: Associativity (number of lines per set)
	 * -b <b>: Number of block bits (B = 2^b is the block size)
	 * -t <tracefile>: Name of the valgrind trace to replay
	 */

    int ch;
    int s = 0, E = 0, b = 0;
    char* tracefile = NULL;
    int help = 0;
    verbose_flag = 0;

    while ((ch = getopt(argc, argv, "hvs:E:b:t:")) != -1)
    {
        switch (ch) {
            case 'h':
                printf("Input Form:\n\n");
                printf("\t./csim [-hv] -s <s> -E <E> -b <b> -t <tracefile>\n\n");
	            printf("-h: Optional help flag that prints usage info\n");
	            printf("-v: Optional verbose flag that displays trace info\n");
	            printf("-s <s>: Number of set index bits (S = 2^s is the number of sets)\n");
	            printf("-E <E>: Associativity (number of lines per set)\n");
	            printf("-b <b>: Number of block bits (B = 2^b is the block size)\n");
	            printf("-t <tracefile>: Name of the valgrind trace to replay\n");
                printf("\nNote that any command with -h flag will only print help\n");
                help = 1;
                break;
            case 'v':
                verbose_flag = 1;
                break;
            case 's':
                s = atoi(optarg);
                break;
            case 'E':
                E = atoi(optarg);
                break;
            case 'b':
                b = atoi(optarg);
                break;
            case 't':
                tracefile = optarg;
                break;
        }
    }

    // If the parameter is for help, stop the simulation.
    if (help == 1) return 0;

    // If any necessary parameter is missed, stop the simulation.
    if (s == 0 || E == 0 || b == 0 || tracefile == NULL) {

        if (s == 0) printf("./csim: s is a mandatory parameter\n");
        if (E == 0) printf("./csim: E is a mandatory parameter\n");
        if (b == 0) printf("./csim: b is a mandatory parameter\n");
        if (tracefile == 0) printf("./csim: t is a mandatory parameter\n");
        printf("Try './csim -h' for more information.\n");

        exit(1);
    }

// ++++++++++++++++++++++++++++ Create Cache ++++++++++++++++++++++++++++++++ //

	cline *cache = create_cache(s, E, b);
	macs record = {0, 0, 0};

// +++++++++++++++++++++++++++++++ Simulate +++++++++++++++++++++++++++++++++ //

    FILE *fin;
    char type = ' ';
    unsigned long long maddr = 0;
    unsigned int msize = 0;
    time_sim = 0;

    fin = fopen(tracefile, "r");

    while (fscanf(fin, " %c %llx,%d\n", &type, &maddr, &msize) != EOF) {
		if (verbose_flag == 1) printf("%c %llx,%d", type, maddr, msize);

		switch (type) {
            case 'I': // only load
                break;
			case 'M': // both load & store
				cache_access(&record, cache, maddr, msize, s, E, b);
			case 'L': // only load
			case 'S': // only store
				cache_access(&record, cache, maddr, msize, s, E, b);
				break;
		}

		if (verbose_flag == 1) printf("\n");
	}

    fclose(fin);

// +++++++++++++++++++++++++++++++ Output +++++++++++++++++++++++++++++++++++ //

    printSummary(record.hits, record.misses, record.evictions);

// +++++++++++++++++++++++++++++ Free Memory ++++++++++++++++++++++++++++++++ //
	free(cache);

    return 0;
}

// +++++++++++++++++++++++ Function Implementation ++++++++++++++++++++++++++ //

/*
 * @brief Create the virtual cache
 * @param s: Number of set index bits (S = 2^s is the number of sets)
 * @param E: Associativity (number of lines per set)
 * @param b: Number of block bits (B = 2^b is the block size)
 * @return The head of the list to a virtual cache
 */
cline* create_cache(int s, int E, int b) {

	// Allocate the memory & clean the content
	cline *cache = calloc((pow(2.0, s) * E), sizeof(cline));

    // Mark the set
    for (int i = 0; i < pow(2.0, s); i++)
        for (int j = 0; j < E; j++) {
            cache[i * E + j].set = i;
            cache[i * E + j].blk = pow(2.0, b);
        }

    // if (verbose_flag == 1) show_cache_content(cache, s, E, b);

	return cache;
}

/*
 * @brief Find a entry in cache using Least-Recently-Used strategy
 * @param cache: The simulated cache
 * @param set: The target set
 * @param s: Number of sets
 * @param E: Number of lines per set
 * @return The target entry
 */
int cache_LRU(cline* cache, int set, int s, int E) {

    int target = 0;
    int entry_num = pow(2.0, s) * E;
    unsigned int oldest_time = 0;

    // First search for the recently referenced time in the cache
    for (int i = 0; i < entry_num; i++) {
        if (cache[i].last_ref > oldest_time)
            oldest_time = cache[i].last_ref;
    }

    // Search for the available entry
    for (int i = 0; i < entry_num; i++) {

        // The target must be in certain set
        if (cache[i].set == set) {

            // If any invalid (available) entry is found, replace it
            if (cache[i].vld == invalid) 
                return i;
            // Else find the least-recently-used entry
            else {
                if (cache[i].last_ref <= oldest_time) {
                    target = i;
                    oldest_time = cache[i].last_ref;
                }
            }
        }
    }

    return target;
}

/*
 * @brief Simulate the load/store instruction on a cache
 * @param record: Record the number of hits, misses & evictions
 * @param cache: The simulated cache
 * @param addr: The target address
 * @param size: The size of memory access
 * @param s: Number of set index bits (S = 2^s is the number of sets)
 * @param E: Associativity (number of lines per set)
 * @param b: Number of block bits (B = 2^b is the block size)
 */
void cache_access(macs* record, cline* cache, unsigned long long addr, 
                unsigned int size, int s, int E, int b) {
	
    // Increase time whenever a cache access occurs
    time_sim = time_sim + 1;

    // Address configuration:
    //
    //              [tag]      [set] [block]
    // Length: (total - b - s)   s      b  
    // Total length is defined as ADDRLEN

	unsigned long long set = 0;
	unsigned long long tag = 0;

// ++++++++++++++++++++++++++ Create Masks ++++++++++++++++++++++++++++++++++ //

    unsigned long long tagMask = 0;
    unsigned long long setMask = 0;

    // Create set mask
    for(int i = 0; i < s; i++){
        setMask = setMask << 1;
        setMask = setMask + 1;
    }

    setMask = setMask << b;

    // Create tag mask
    for(int i = 0; i < ADDRLEN - (b + s); i++){
        tagMask = tagMask << 1;
        tagMask = tagMask + 1;
    }

    tagMask = tagMask << (b + s);

// ++++++++++++++++++++++++++++++ Get Info ++++++++++++++++++++++++++++++++++ //

    set = (addr & setMask) >> b;
    tag = (addr & tagMask) >> (b + s);

    // if (verbose_flag == 1) printf("\n%0*llx %0*llx", 
    //                               (s / 4),                 set,
    //                               ((ADDRLEN - s - b) / 4), tag);

// ++++++++++++++++++++++++++++ Access Cache ++++++++++++++++++++++++++++++++ //

    int entry_num = pow(2.0, s) * E;
    int exist = 0;

    // Search for existance
    for (int i = 0; i < entry_num; i++) {

        // The target must be in certain set
        if (cache[i].set == set) {
            if (cache[i].vld == valid && cache[i].tag == tag) {
                exist = 1;
                record->hits += 1;
                cache[i].last_ref = time_sim; // refresh the reference time

                if (verbose_flag == 1) printf(" hit");
                break;
            }
        }
    }

    // If cache missed
    if (exist == 0) {
        if (verbose_flag == 1) printf(" miss");

        int target = cache_LRU(cache, set, s, E);

        // Record the number of evictions if evict some entry by covering
        if (cache[target].vld == valid) {
            if (verbose_flag == 1) printf(" evicition");
            record->evictions += 1;
        }
        else
            cache[target].vld = valid;

        cache[target].tag = tag;
        cache[target].last_ref = time_sim;
        record->misses += 1;
    }


    // if (verbose_flag == 1) show_cache_content(cache, s, E, b);
}

/*
 * @brief Show the content of the simulated cache on screen
 * @param cache: The simulated cache
 * @param s: Number of set index bits (S = 2^s is the number of sets)
 * @param E: Associativity (number of lines per set)
 * @param b: Number of block bits (B = 2^b is the block size)
 */
void show_cache_content(cline* cache, int s, int E, int b) {
    printf("\n[valid] [set] [tag] [block] [time]\n");

    int entry_num = pow(2.0, s) * E;

    for (int i = 0; i < entry_num; i++) {
        printf((cache[i].vld == valid) ? "v " : "i ");
        printf("%0*llx %0*llx %0*lld %d\n", 
               (s / 4),                 cache[i].set,
               ((ADDRLEN - s - b) / 4), cache[i].tag, 
               (b / 4),                 cache[i].blk,
                                        cache[i].last_ref);
    }
}

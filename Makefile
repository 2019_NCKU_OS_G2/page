OS_NAME = $(shell uname -s)

ifeq ($(OS_NAME), Linux)
CC := g++
endif

ifeq ($(OS_NAME), Darwin)
CC := g++-8
endif

exe := page
obj := page.o


all: $(exe)

$(exe):$(obj)
	$(CC) -o $@ $@.o
%.o: %.cpp
	$(CC) -c $^ -o $@


exec:
	make
	./$(exe)

.PHONY:clean

clean:
	rm -rf $(obj) $(exe) *.out group2_ans.txt *.csv
